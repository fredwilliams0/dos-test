/*
	Created: RAP - December 2016 for first release
	Purpose: provide context for Lien Detail Lightning Components 
*/
global with sharing class LienController_Aura {

//	public static final strings

//	other variables
	public static string retURL;
	public static boolean editable{get{if(editable==null)return false;return editable;}set;}
	public static boolean fault{get;set;}
	public static string acctStreet{get;set;}
	public static string acctCityStateZip{get;set;}
	public static string selectedLienAmt{get;set;}
	
	public static string lienId {
		get {
			if (string.isBlank(lienId))
				lienId = ApexPages.currentPage().getParameters().get('id');
			return lienId;
		}
		set;
	}
	public static string cancelReturn {
		get {
			if (editable)
				return 'Cancel';
			return 'Return';
		}
	}
	global static list<Lien__c> liens {
		get {
			if (liens == null && lienId != null) {
				liens = [SELECT Account__c, Action__c, Claimant__c, Cleared__c, Date_No_Interest__c, Date_Payment__c, Substage__c, 
							    Date_Re_sweep_Sent__c, Date_Submitted__c, Deal_Made__c, Eligible__c, Final_Demand_Amount__c,
							    Final_Demand_Received__c, Final_Lien_Amt_Paid__c, General_Ledger__c, Health_Plan__c, HICN__c,
							    Id, LienAmount__c, Lien_Holder_File_ID__c, Lien_Type__c, Name, Notes__c, Payable_Entity__c,
							    Policy__c, Re_sweep_Sent__c, Stages__c, State__c, Status_ERISA__c, Status__c, Submitted__c,
							    Subro_Rep__c, Claimant__r.Name, Account__r.Lienholder_Contact__r.Name, Account__r.Name, 
							    Account__r.Lienholder_Contact__r.Email, Account__r.Lienholder_Contact__r.Phone, Claimant__r.DOL__c, 
							    Account__r.Lienholder_Contact__r.Fax, Account__r.BillingStreet, Account__r.BillingCity, 
							    Account__r.BillingState, Account__r.BillingPostalCode, 
							    (SELECT Date_End__c, Date_Start__c, Id, LienType__c
							     FROM LienEligibilityDates__r
							     order by Date_Start__c desc)
						 FROM Lien__c];
//						 WHERE Id = :lienId];	
			}
			return liens;
		}
		set;
	}
	public static Lien__c lien {
		get {
			if (lien == null) {
				if (liens != null && !liens.isEmpty())
					lien = liens[0];
			}
			return lien;
		}
		set;
	}
	public static Lien_Negotiated_Amounts__c lienAmt {
		get {
			if (lienAmt == null) {	
				if (lienAmtList != null && !lienAmtList.isEmpty()) {
					Date latest = lienAmtList[0].Lien_Amount_Date__c;
					lienAmt = lienAmtList[0];
					for (Lien_Negotiated_Amounts__c lnac : lienAmtList) {
						if (lnac.Lien_Amount_Date__c > latest) {
							latest = lnac.Lien_Amount_Date__c;
							lienAmt = lnac;
						}
					}
				}
			}
			return lienAmt;
		}
		set;
	}
	public static list<Lien_Negotiated_Amounts__c> lienAmtList {
		get {
			if (lienAmtList == null) {
				lienAmtList = [SELECT Id, Lien__c, Lien_Amount__c, Lien_Amount_Date__c, Phase__c
							   FROM Lien_Negotiated_Amounts__c
							   WHERE Lien__c = :lien.Id
							   order by Lien_Amount_Date__c asc];
			}
			return lienAmtList;
		}
		set;
	}	
// constructor
	public LienController_Aura(ApexPages.StandardController sc) {
		fault = false;
		lienId = sc.getId();
		retURL = ApexPages.currentPage().getParameters().get('returl');
		if (string.isBlank(lienId)) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Missing URL parameter. Please contact your system administrator.'));
			fault = true;
		}
		else {
			acctStreet = lien.Account__r.BillingStreet;
			acctCityStateZip = lien.Account__r.BillingCity + ', ' + lien.Account__r.BillingState + '  ' + lien.Account__r.BillingPostalCode;
		}
	}
	
// action methods
@auraEnabled
	public static string Save() {
		if (!Validate())
			return 'Error';
		database.saveResult sr;
		try {
			sr = database.update(lien);
		}
		catch (exception e) {
			throw new AuraHandledException('Exception caught saving Lien: ' + e.getMessage());
		}
		if (sr != null) {
			if (sr.isSuccess())
				editable = false;
			else {
				throw new AuraHandledException('Lien save failed: ' + sr.getErrors());
			}
		}
		return 'Success';
	}
	
	private static boolean Validate() {
		boolean isValid = true;
		return isValid;
	}
	
@auraEnabled
	global static Lien__c getLiens(string lId) {
		return [SELECT Account__c, Action__c, Claimant__c, Cleared__c, Date_No_Interest__c, Date_Payment__c, Substage__c, 
					   Date_Re_sweep_Sent__c, Date_Submitted__c, Deal_Made__c, Eligible__c, Final_Demand_Amount__c,
					   Final_Demand_Received__c, Final_Lien_Amt_Paid__c, General_Ledger__c, Health_Plan__c, HICN__c,
					   Id, LienAmount__c, Lien_Holder_File_ID__c, Lien_Type__c, Name, Notes__c, Payable_Entity__c,
					   Policy__c, Re_sweep_Sent__c, Stages__c, State__c, Status_ERISA__c, Status__c, Submitted__c,
					   Subro_Rep__c, Claimant__r.Name, Account__r.Lienholder_Contact__r.Name, Account__r.Name, 
					   Account__r.Lienholder_Contact__r.Email, Account__r.Lienholder_Contact__r.Phone, Claimant__r.DOL__c, 
					   Account__r.Lienholder_Contact__r.Fax, Account__r.BillingStreet, Account__r.BillingCity, 
					   Account__r.BillingState, Account__r.BillingPostalCode, 
					   (SELECT Date_End__c, Date_Start__c, Id, LienType__c
					    FROM LienEligibilityDates__r
					    order by Date_Start__c desc)
				FROM Lien__c 
				WHERE Id = :lId
				limit 1];
	}
@auraEnabled 
	public static list<Lien_Negotiated_Amounts__c> getLienAmtList(string lId) {
		return [SELECT Id, Lien__c, Lien_Amount__c, Lien_Amount_Date__c, Phase__c
				FROM Lien_Negotiated_Amounts__c
				WHERE Lien__c = :lId
				order by Lien_Amount_Date__c desc];
	}	
	
@auraEnabled
	public static list<Injury__c> getInjuries(string lId) {
		Lien__c l = [SELECT Action__c, Claimant__c FROM Lien__c WHERE Id = :lId];
		return [SELECT Action__c, Compensable__c, Claimant__c, Description_Long__c, Explant_Date__c, ICD_Code__c, Id, Implant_Date__c, Injury_Category__c,
					   Injury_Description__c, Last_Treatment_Date__c, Lien__c, Name, Non_Surgical_Treatment_Enhancer__c, Non_Surgical_Treatment__c,
					   Surgical_Facility__c, Surgical_Treatment_Enhancer__c, Total_Surgery_Per_Defendant__c, Treatment_Enhancer__c, Base_Category__c,
					   Settlement_Category__c
				FROM Injury__c
				WHERE Claimant__c = :l.Claimant__c
				AND Action__c = :l.Action__c];	
	}	
@auraEnabled
	public static list<Award__c> getAwards(string lId) {
		Lien__c l = [SELECT Action__c, Claimant__c FROM Lien__c WHERE Id = :lId];
		return [SELECT Id, Action__c, Claimant__c, Date_of_Award__c, Amount__c, Claimant__r.Name
				FROM Award__c
				WHERE Claimant__c = :l.Claimant__c
				AND Action__c = :l.Action__c];
	}
	
@auraEnabled
	public static string EditLHA() {
		return '/' + selectedLienAmt;
	}
	
@auraEnabled
	public static void DeleteLHA() {
		Lien_Negotiated_Amounts__c lnac = [SELECT Id FROM Lien_Negotiated_Amounts__c WHERE Id = :selectedLienAmt];
		delete lnac;
		lienAmtList = null;
	}
	
@auraEnabled
	public static string Cancel() {
		return string.isBlank(retURL) ? '/home/home.jsp' : retURL;
	}
}