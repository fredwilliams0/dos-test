/*
	Created: RAP - December 2016 for first release
	Purpose: provide context for Lien summary and detail pages 
*/

public with sharing class LienController {
//	public static final strings

//	other variables
	public boolean isValid;
	public string retURL;
	public boolean editable{get{if(editable==null)return false;return editable;}set;}
	public boolean fault{get;set;}
	public string acctStreet{get;set;}
	public string acctCityStateZip{get;set;}
	public string selectedLienAmt{get;set;}
	public string selectedNote{get;set;}
	public string selectedAct{get;set;}
	public string selectedInj{get;set;}
	public string selectedAwrd{get;set;}
	
	public string lienId {
		get {
			if (string.isBlank(lienId))
				lienId = ApexPages.currentPage().getParameters().get('id');
			return lienId;
		}
		set;
	}
	public string cancelReturn {
		get {
			if (editable)
				return 'Cancel';
			return 'Return';
		}
	}
	public list<Lien__c> liens {
		get {
			if (liens == null && lienId != null) {
				liens = [SELECT Account__c, Action__c, Claimant__c, Cleared__c, Date_No_Interest__c, Date_Payment__c, Substage__c, 
							    Date_Re_sweep_Sent__c, Date_Submitted__c, Deal_Made__c, Eligible__c, Final_Demand_Amount__c,
							    Final_Demand_Received__c, Final_Lien_Amt_Paid__c, General_Ledger__c, Health_Plan__c, HICN__c,
							    Id, LienAmount__c, Lien_Holder_File_ID__c, Lien_Type__c, Name, Notes__c, Payable_Entity__c,
							    Policy__c, Re_sweep_Sent__c, Stages__c, State__c, Status_ERISA__c, Status__c, Submitted__c,
							    Subro_Rep__c, Claimant__r.Name, Account__r.Lienholder_Contact__r.Name, Account__r.Name, 
							    Account__r.Lienholder_Contact__r.Email, Account__r.Lienholder_Contact__r.Phone, Claimant__r.DOL__c, 
							    Account__r.Lienholder_Contact__r.Fax, Account__r.BillingStreet, Account__r.BillingCity, 
							    Account__r.BillingState, Account__r.BillingPostalCode, Action__r.Name,
							    (SELECT Date_End__c, Date_Start__c, Id, LienType__c
							     FROM LienEligibilityDates__r
							     order by Date_Start__c desc)
						 FROM Lien__c
						 WHERE Id = :lienId];	
			}
			return liens;
		}
		set;
	}
	public Lien__c lien {
		get {
			if (lien == null) {
				if (liens != null && !liens.isEmpty())
					lien = liens[0];
			}
			return lien;
		}
		set;
	}
	public Lien_Negotiated_Amounts__c lienAmt {
		get {
			if (lienAmt == null) {	
				if (lienAmtList != null && !lienAmtList.isEmpty()) {
					Date latest = lienAmtList[0].Lien_Amount_Date__c;
					lienAmt = lienAmtList[0];
					for (Lien_Negotiated_Amounts__c lnac : lienAmtList) {
						if (lnac.Lien_Amount_Date__c > latest) {
							latest = lnac.Lien_Amount_Date__c;
							lienAmt = lnac;
						}
					}
				}
			}
			return lienAmt;
		}
		set;
	}
	public list<Lien_Negotiated_Amounts__c> lienAmtList {
		get {
			if (lienAmtList == null) {
				try {
					lienAmtList = [SELECT Id, Lien__c, Lien_Amount__c, Lien_Amount_Date__c, Phase__c
								   FROM Lien_Negotiated_Amounts__c
								   WHERE Lien__c = :lien.Id
								   order by Lien_Amount_Date__c asc];
				}
				catch (exception e) {}
			}
			return lienAmtList;
		}
		set;
	}	
	public list<Note> noteList {
		get {
			if (noteList == null) {
				noteList = [SELECT Id, Body, Title, Owner.Name, CreatedDate
							FROM Note
							WHERE parentId = :lien.Id
							order by CreatedDate desc];
			}
			return noteList;
		}
		set;
	}
	public list<Injury__c> injuries {
		get {
			if (injuries == null) {
				injuries = [SELECT Action__c, Compensable__c, Claimant__c, Description_Long__c, Explant_Date__c, ICD_Code__c, Id, Implant_Date__c, Injury_Category__c,
								   Injury_Description__c, Last_Treatment_Date__c, Lien__c, Name, Non_Surgical_Treatment_Enhancer__c, Non_Surgical_Treatment__c,
								   Surgical_Facility__c, Surgical_Treatment_Enhancer__c, Total_Surgery_Per_Defendant__c, Treatment_Enhancer__c, Base_Category__c,
								   Settlement_Category__c
							FROM Injury__c
							WHERE Claimant__c = :lien.Claimant__c
							AND Action__c = :lien.Action__c];	
			}
			return injuries;
		}
		set;
	}
	public list<Award__c> awrdList {
		get {
			if (awrdList == null) {
				awrdList = new list<Award__c>();
				awrdList.addAll([SELECT Id, Action__c, Claimant__c, Date_of_Award__c, Amount__c, Claimant__r.Name
								 FROM Award__c
								 WHERE Claimant__c = :lien.Claimant__c
								 AND Action__c = :lien.Action__c]);
			}
			return awrdList;
		}
		set;
	}
	public list<Task> actList {
		get {
			if (actList == null) {
				actList = new list<Task>();
				actList.addAll([SELECT Id, Subject, Owner.Name, Type
								FROM Task
								WHERE WhatId = :lien.Id]);
			}
			return actList;
		}
		set;
	}
// constructor
	public LienController() {
		fault = false;
		retURL = ApexPages.currentPage().getParameters().get('returl');
		if (string.isBlank(lienId)) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Missing URL parameter. Please contact your system administrator.'));
			fault = true;
		}
		else {
			acctStreet = lien.Account__r.BillingStreet;
			acctCityStateZip = lien.Account__r.BillingCity + ', ' + lien.Account__r.BillingState + '  ' + lien.Account__r.BillingPostalCode;
			editable = lien.Stages__c != 'Final';
		}
	}
	
// action methods
	public pageReference Save() {
		Validate();
		if (!isValid)
			return null;
		database.saveResult sr;
		try {
			sr = database.update(lien);
		}
		catch (exception e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Exception caught saving Lien: ' + e.getMessage()));
		}
		if (sr != null) {
			if (sr.isSuccess())
				editable = false;
			else
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Lien save failed: ' + sr.getErrors()));
		}
		return null;
	}
	
	public void Validate() {
		isValid = true;
	}

	public pageReference Edit() {
		editable = true;
		return null;
	}
	
	public pageReference Cancel() {
		pageReference pg;
		if (!string.isBlank(retURL))
			pg = new pageReference(retURL);
		else
			pg = new pageReference('/home/home.jsp');
		return pg.setRedirect(true);
	}
// Lien Negotiated Amounts methods	
	public pageReference NewLHA() {
		Lien_Negotiated_Amounts__c l = new Lien_Negotiated_Amounts__c(Lien__c = lien.Id);
		insert l;
		pageReference pg = new pageReference('/' + l.Id + '/e?retUrl=/apex/LienDetail?id=' + lien.Id);
		return pg.setRedirect(true);
	}

	public pageReference EditLHA() {
		pageReference pg = new pageReference('/' + selectedLienAmt + '/e?retUrl=/apex/LienDetail?id=' + lien.Id);
		return pg.setRedirect(true);
	}
	
	public pageReference DeleteLHA() {
		Lien_Negotiated_Amounts__c lnac = [SELECT Id FROM Lien_Negotiated_Amounts__c WHERE Id = :selectedLienAmt];
		delete lnac;
		lienAmtList = null;
		return null;
	}
// Note Methods	
	public pageReference NewNote() {
		Note n = new Note(parentId=lien.Id, Title='Enter Title');
		insert n;
		pageReference pg = new pageReference('/' + n.Id + '/e?parentId=' + lien.Id + '&retUrl=/apex/LienDetail?id=' + lien.Id);
		return pg.setRedirect(true);
	}

	public pageReference EditNote() {
		pageReference pg = new pageReference('/' + selectedNote + '/e?parentId=' + lien.Id + '&retUrl=/apex/LienDetail?id=' + lien.Id);
		return pg.setRedirect(true);
	}
	
	public pageReference DeleteNote() {
		Note n = [SELECT Id FROM Note WHERE Id = :selectedNote];
		delete n;
		noteList = null;
		return null;
	}
// tabPanel - Activity Methods
	public pageReference NewActivity() {
		pageReference pg = new pageReference('/00T/e?what_id=' + lien.Id + '&retUrl=/apex/LienDetail?id=' + lien.Id);
		return pg.setRedirect(true);
	}
	
	public pageReference EditAct() {
		pageReference pg = new pageReference('/' + selectedAct + '/e?retUrl=/apex/LienDetail?id=' + lien.Id);
		return pg.setRedirect(true);
	}
	
	public pageReference DeleteAct() {
		Task t = [SELECT Id FROM Task WHERE Id = :selectedAct];
		delete t;
		actList = null;
		return null;
	}
// tabPanel - Award Methods
	public pageReference NewAward() {
		Award__c a = new Award__c(Action__c = lien.Action__c, Claimant__c = lien.Claimant__c);
		insert a;
		pageReference pg = new pageReference('/' + a.Id + '/e?retUrl=/apex/LienDetail?id=' + lien.Id);
		return pg.setRedirect(true);
	}
	
	public pageReference EditAwrd() {
		pageReference pg = new pageReference('/' + selectedAwrd + '/e?retUrl=/apex/LienDetail?id=' + lien.Id);
		return pg.setRedirect(true);
	}
	
	public pageReference DeleteAwrd() {
		Award__c a = [SELECT Id FROM Award__c WHERE Id = :selectedAwrd limit 1];
		delete a;
		awrdList = null;
		return null;
	}
// tabPanel - Injury Methods
	public pageReference NewInjury() {
		Injury__c i = new Injury__c(Name = 'Enter Name',
									Action__c = lien.Action__c, 
									Claimant__c = lien.Claimant__c, 
									Lien__c = lien.Id);
		insert i;
		pageReference pg = new pageReference('/' + i.Id + '/e?retUrl=/apex/LienDetail?id=' + lien.Id);
		return pg.setRedirect(true);
	}
	
	public pageReference EditInj() {
		pageReference pg = new pageReference('/' + selectedInj + '/e?retUrl=/apex/LienDetail?id=' + lien.Id);
		return pg.setRedirect(true);
	}
	
	public pageReference DeleteInj() {
		Injury__c i = [SELECT Id FROM Injury__c WHERE Id = :selectedInj limit 1];
		delete i;
		injuries = null;
		return null;
	}
// Flow Graphic methods	
	public void AdvanceStage() {
		string stage = lien.Stages__c;
		if (stage == 'To Be Submitted')
			lien.Stages__c = 'Submitted';
		else if (stage == 'Submitted')
			lien.Stages__c = 'No Lien';
		else if (stage == 'No Lien')
			lien.Stages__c = 'Asserted';
		else if (stage == 'Asserted')
			lien.Stages__c = 'Audit';
		else if (stage == 'Audit')
			lien.Stages__c = 'Final';
		update lien;
	}
}