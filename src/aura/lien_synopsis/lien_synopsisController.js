({

    doInit : function(component, event, helper) {
        var action = component.get("c.getLiens");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lien", response.getReturnValue());
            } else {
                console.log('Problem getting lien, response state: ' + state);
            }
        });
        var action2 = component.get("c.getLienAmtList");
        action2.setParams({"lId": component.get("v.recordId")});
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.lienAmts", response.getReturnValue());
            } else {
                console.log('Problem getting lien Amounts, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
        $A.enqueueAction(action2);
    }
})