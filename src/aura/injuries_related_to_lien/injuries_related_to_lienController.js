({

    doInit : function(component, event, helper) {
        var action = component.get("c.getInjuries");
        action.setParams({"lId": component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.injuries", response.getReturnValue());
            } else {
                console.log('Problem getting injuries, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    }

,

    showModal : function(component, event, helper) {
        document.getElementById("backGroundSectionId").style.display = "block";
        document.getElementById("injuryEditId").style.display = "block";
    }
,
    showDropdown : function(component, event, helper) {
        document.getElementById("dropDown").style.display = "block";
    }
})